import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Alert } from '@bootstrap-styled/v4';
import { request } from '../../utils/request';
import Layout from '../../components/Common/Layout';
import Button from '../../components/Common/Button';
import {
	StyledAlertList,
	StyledCheckIcon,
	StyledCancelIcon,
	StyledFormGroup,
	StyledInfoText,
	StyledFieldWrapper,
	StyledLabel
} from './styles';
import routes from '../../constants/routes';
import buttons from '../../constants/buttons';
import { SettingsContext } from '../../contexts/SettingsContext';
import { FormsContext } from '../../contexts/FormsContext';
import Breadcrumbs from '../../components/Common/Breadcrumb';
import TextField from '../../components/Common/TextField';
import Warning from '../../components/Common/Warning';
import emailIcon from '../../assets/icons/email.svg';
import emailErrorIcon from '../../assets/icons/emailError.svg';
import emailSuccessIcon from '../../assets/icons/emailSuccess.svg';
import ControlPanel from '../../components/Common/ControlPanel';
import AlertList from '../../components/Common/AlertList';

const StyledMainContainer = styled.div`
	outline: none;
`;

const validateEmail = (email) => {
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
};

const SettingsPage = () => {
	const [apiKey, setApiKey] = useState('');
	const [organizationId, setOrganizationId] = useState('');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [externalSystemName, setExternalSystemName] = useState('');
	const [email, setEmail] = useState('');
	const [isEmailValid, setIsEmailValid] = useState(false);
	const [isLoading, setIsLoading] = useState(true);

	const [errorText, setErrorText] = useState('Error while saving settings');
	const [isErrorShown, setIsErrorShown] = useState(false);
	const [isDataSaved, setIsDataSaved] = useState(false);
	const history = useHistory();
	const { hasAccess, updateSettingsList } = useContext(SettingsContext);
	const formsContext = useContext(FormsContext);

	const checkEmail = (value) => {
		let isValid = true;
		const emailList = value.split(',');
		for (let item of emailList) {
			isValid = validateEmail(item);
			if (!isValid) {
				break;
			}
		}
		setIsEmailValid(isValid);
	};

	useEffect(() => {
		request
			.get(`/settings`)
			.then((response) => {
				const { data } = response;
				if (Object.keys(data).length > 0) {
					setApiKey(data.devicemagicUsername);
					setOrganizationId(data.devicemagicOrganizationId);
					setUsername(data.serviceTradeUserName);
					setPassword(data.serviceTradePassword);
					setExternalSystemName(data.serviceTradeExternalSystemName);
					if (data.email) {
						setEmail(data.email);
						checkEmail(data.email);
					}
				}
			})
			.catch((error) => {
				setErrorText(error.message);
				setIsErrorShown(true);
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, []);

	const save = async () => {
		setIsLoading(true);
		try {
			const response = await request.post('/settings', {
				apiKey,
				organizationId,
				username,
				password,
				externalSystemName,
				email
			});
			setIsDataSaved(true);
			await updateSettingsList(response.data);
			formsContext.updateFormsList();
			setTimeout(() => {
				history.push(routes.SERVICE_FORMS);
			}, 1000);
		} catch (error) {
			setErrorText(error.message);
			setIsErrorShown(true);
		}

		setIsLoading(false);
	};

	const onEmailChange = (e) => {
		const value = e.target.value;
		setEmail(value);

		if (value) {
			checkEmail(value);
		}
	};

	const saveDisabled =
		isLoading ||
		!apiKey ||
		!organizationId ||
		!username ||
		!password ||
		!externalSystemName ||
		(email ? !isEmailValid : false) ||
		isDataSaved;

	const onKeyDown = (e) => {
		if (!saveDisabled && e.key === buttons.ENTER) {
			save();
		}
	};

	return (
		<Layout.Container>
			<Layout.Content>
				{hasAccess ? <Breadcrumbs url={routes.ROOT} title={'Forms'} /> : null}

				<StyledMainContainer onKeyDown={onKeyDown} tabIndex={0}>
					<AlertList>
						<Alert
							isOpen={isErrorShown}
							color={'danger'}
							autoHideDuration={3000}
							onClick={() => setIsErrorShown(false)}
						>
							{errorText}
						</Alert>
						<Alert
							isOpen={isDataSaved}
							color={'success'}
							autoHideDuration={3000}
							onClick={() => setIsDataSaved(false)}
						>
							{'Settings saved'}
						</Alert>
					</AlertList>
					<Layout.Title title={'Settings'} />

					{hasAccess ? null : (
						<Warning>
							<p>Set your credentials to start using ServiceForms</p>
						</Warning>
					)}

					<h2>Device Magic</h2>

					<StyledFormGroup>
						<StyledLabel for="apiKey">API key</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								type="text"
								name="apiKey"
								id="apiKey"
								placeholder=""
								value={apiKey}
								onChange={(e) => setApiKey(e.target.value)}
							/>
						</StyledFieldWrapper>
					</StyledFormGroup>

					<StyledFormGroup>
						<StyledLabel for="orgId">Org Id</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								type="text"
								name="orgId"
								id="orgId"
								placeholder=""
								value={organizationId}
								onChange={(e) => setOrganizationId(e.target.value)}
							/>
						</StyledFieldWrapper>
					</StyledFormGroup>

					<StyledFormGroup>
						<StyledLabel for="externalSystemName">External system name</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								type="text"
								name="externalSystemName"
								id="externalSystemName"
								placeholder=""
								value={externalSystemName}
								onChange={(e) => setExternalSystemName(e.target.value)}
							/>
						</StyledFieldWrapper>
					</StyledFormGroup>

					<h2>ServiceTrade</h2>

					<StyledFormGroup>
						<StyledLabel for="userName">Username</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								type="text"
								name="userName"
								id="userName"
								placeholder=""
								value={username}
								onChange={(e) => setUsername(e.target.value)}
							/>
						</StyledFieldWrapper>
					</StyledFormGroup>

					<StyledFormGroup>
						<StyledLabel for="password">Password</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								type="password"
								name="password"
								id="password"
								placeholder=""
								value={password}
								onChange={(e) => setPassword(e.target.value)}
							/>
						</StyledFieldWrapper>
					</StyledFormGroup>

					<h2>General</h2>

					<StyledFormGroup>
						<StyledLabel for="userName">Failure Notification Email</StyledLabel>

						<StyledFieldWrapper>
							<TextField
								icon={
									<img
										src={
											isEmailValid && email
												? emailSuccessIcon
												: email
												? emailErrorIcon
												: emailIcon
										}
									/>
								}
								type="text"
								name="email"
								id="email"
								placeholder=""
								value={email}
								onChange={onEmailChange}
							/>
							<StyledInfoText>
								Notifications about destination rule failures will be delivered
								here. Add one or more email addresses (separated by commas), or
								leave blank to disable notification.
							</StyledInfoText>
						</StyledFieldWrapper>
					</StyledFormGroup>
				</StyledMainContainer>
			</Layout.Content>

			<ControlPanel>
				<Button color="primary" type="submit" disabled={saveDisabled} onClick={save}>
					Save Settings
				</Button>
			</ControlPanel>
		</Layout.Container>
	);
};
export default SettingsPage;
