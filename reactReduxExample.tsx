import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Loader, toast } from "rsv-components";
import { CustomFields } from "../CustomFields/index";
import {
    register,
    withContent,
    withResource,
    withTheme
} from "xcel-react-core";
import {
    getCustomFieldsByFormTypeAndKey,
    getUserCustomFieldByFormTypeAndKey,
    updateUserCustomFields
} from "../../redux/actions";
import {
    getFieldsByGroup,
    getCustomFieldsFormInitialValues
} from "../../redux/selectors";
import { ensureArray } from "xcel-util";
import { reduxForm, formSelectors } from "rsv-forms";
import styled from "styled-components";

const StyledButton = styled(Button)`
  margin-top: 20px;
`;

const StyledLoader = styled(Loader)`
  padding: 11px;
`;

export interface CustomFieldsProps {
    formType: string;
    groupKey: string;
    submitButtonText?: string;
    submitButtonThemeVariation?: string;
    isReadOnly?: boolean;
    formValues: any;
    isFormValid: boolean;
    initialValues: any;
    [otherProps: string]: any;
}

interface InternalProps extends CustomFieldsProps {
    fields: Array<any>;
    actions: any;
    isLoading: false;
}

let CustomForm = props => {
    return (
        <form
            noValidate={true}
            className={props.formClassName}
            onSubmit={e => {
                if (props.handleSubmit) {
                    props.handleSubmit(e);
                }
            }}
        >
            <CustomFields customFields={props.fields} />
            {props.isSubmitting ? <StyledLoader /> : null}
            {props.isReadOnly || props.isSubmitting ? null : (
                <StyledButton
                    type="submit"
                    themeVariation={
                        props.submitButtonThemeVariation
                            ? props.submitButtonThemeVariation
                            : "primary"
                    }
                    disabled={props.allFieldsNotEditable || !props.isFormValid}
                    className={"submit-button"}
                >
                    {props.submitButtonText
                        ? props.submitButtonText
                        : props.submitButtonLabel}
                </StyledButton>
            )}
        </form>
    );
};

class CustomFieldsForm extends React.Component<
    InternalProps,
    {
        isLoading: boolean;
        isSubmitting: boolean;
    }
    > {
    customForm = null;

    constructor(props: InternalProps) {
        super(props);
        this.state = {
            isLoading: false,
            isSubmitting: false
        };
    }

    validate = values => {
        const errors = {};
        return errors;
    };

    componentDidMount() {
        const { formName } = this.props;

        this.getData(this.props).then(() => {
            const mapStateToProps = state => {
                return {
                    initialValues: { form: this.props.initialValues }
                };
            };
            this.customForm = reduxForm({
                form: formName,
                validate: this.validate
            })(CustomForm);

            this.customForm = connect(mapStateToProps)(this.customForm);

            this.forceUpdate();
        });
    }

    getData = async ({ formType, groupKey }: InternalProps) => {
        const { actions } = this.props;
        if (formType && groupKey) {
            this.setState({
                isLoading: true
            });
            await actions.getCustomFieldsByFormTypeAndKey({
                formType,
                groupKey: ensureArray(groupKey),
                appKey: "empty"
            } as any);
            await actions.getUserCustomFieldByFormTypeAndKey({
                formType,
                groupKey: ensureArray(groupKey),
                appKey: "empty"
            } as any);
            this.setState({
                isLoading: false
            });
        }
    };

    createFieldModel = (fieldId, value) => {
        return {
            customFieldKey: fieldId,
            value
        };
    };

    handleSubmit = async e => {
        e.preventDefault();
        const {
            formType,
            groupKey,
            actions,
            formValues,
            isFormValid,
            fields,
            toastSuccessMessage,
            toastFailMessage
        } = this.props;
        if (!isFormValid) {
            return;
        }

        const values = formValues["form"];
        let models = [];
        for (let field of fields) {
            const value = values[field.id];
            if (value) {
                if (Array.isArray(value)) {
                    if (value.length) {
                        value.map(val =>
                            models.push(this.createFieldModel(field.id, val))
                        );
                    } else {
                        models.push(this.createFieldModel(field.id, ""));
                    }
                } else {
                    models.push(this.createFieldModel(field.id, value));
                }
            } else {
                models.push(this.createFieldModel(field.id, ""));
            }
        }

        try {
            this.setState({
                isSubmitting: true
            });
            await actions.updateUserCustomFields({
                formType,
                key: groupKey,
                models
            });
            toast(
                {
                    template: "Default",
                    message: toastSuccessMessage
                },
                { autoClose: true } as any
            );
        } catch (e) {
            toast(
                {
                    template: "Default",
                    themeVariation: "error-text",
                    message: toastFailMessage
                },
                { autoClose: true } as any
            );
        } finally {
            this.setState({
                isSubmitting: false
            });
        }
    };

    render() {
        const { fields } = this.props;
        const { isLoading, isSubmitting } = this.state;
        const allFieldsNotEditable =
            fields.filter(item => item.readOnly).length === fields.length;
        const Form = this.customForm;
        return (
            <div className={"form-container"}>
                {isLoading ? <Loader /> : null}
                {!fields || !fields.length || isLoading || !Form ? null : (
                    <Form
                        {...{
                            ...this.props,
                            isLoading,
                            isSubmitting,
                            handleSubmit: this.handleSubmit,
                            allFieldsNotEditable
                        }}
                    />
                )}
            </div>
        );
    }
}

const mapState = (state, ownProps) => {
    const fields = getFieldsByGroup(
        state,
        ensureArray(ownProps.groupKey),
        ownProps.formType,
        "empty"
    );

    const initialValues = getCustomFieldsFormInitialValues(state, [
        ownProps.formType,
        "empty",
        ownProps.groupKey
    ]);

    for (let field of fields) {
        if (ownProps.customFieldLabelTextThemeVariation) {
            field.labelTextThemeVariation =
                ownProps.customFieldLabelTextThemeVariation;
        }
        if (ownProps.customFieldLabelThemeVariation) {
            field.labelThemeVariation = ownProps.customFieldLabelThemeVariation;
        }
        if (field.readOnly) {
            continue;
        }
        if (
            initialValues &&
            initialValues[field.id] &&
            !field.isEditablePostSubmission
        ) {
            field.readOnly = true;
        }
    }

    return {
        fields: fields,
        formValues: formSelectors.getFormValues(ownProps.formName)(state),
        isFormValid: formSelectors.isValid(ownProps.formName)(state),
        initialValues: initialValues
    };
};

const mapDispatch = dispatch => ({
    actions: bindActionCreators(
        {
            getCustomFieldsByFormTypeAndKey,
            updateUserCustomFields,
            getUserCustomFieldByFormTypeAndKey
        },
        dispatch
    )
});

const mapResourceToProps = getResource => {
    return {
        submitButtonLabel: getResource(
            "customFieldsForm.submitButtonText",
            "Submit"
        ),
        toastSuccessMessage: getResource(
            "customFieldsForm.toastSuccessMessage",
            "Form saved"
        ),
        toastFailMessage: getResource(
            "customFieldsForm.toastFailMessage",
            "Failed save form"
        ),
    };
};

const mapContentToProps = getContent => {
    const formName = getContent("formName", { type: "none" });
    return {
        formType: getContent("formType", { type: "none" }),
        groupKey: getContent("groupKey", { type: "none" }),
        formName: formName ? `${formName}_${+new Date()}` : +new Date(),
        submitButtonText: getContent("submitButtonText", {
            type: "none",
            label: "Submit button text"
        }),
        submitButtonThemeVariation: getContent("submitButtonThemeVariation", {
            type: "none"
        }),
        customFieldLabelTextThemeVariation: getContent(
            "customFieldLabelTextThemeVariation",
            {
                type: "none"
            }
        ),
        customFieldLabelThemeVariation: getContent(
            "customFieldLabelThemeVariation",
            {
                type: "none"
            }
        ),
        formClassName: getContent("formClassName", {
            type: "none"
        }),
        isReadOnly: getContent("isReadOnly", {
            type: "checkbox",
            label: "Read only"
        })
    };
};

export default register("customfields/CustomFieldsForm")(
    withContent(mapContentToProps),
    withResource(mapResourceToProps),
    connect(
        mapState,
        mapDispatch
    ),
    withTheme([".form-container", ".submit-button"])
)(CustomFieldsForm);
