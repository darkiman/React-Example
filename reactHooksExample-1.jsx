import React, { useState, useMemo, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Input, Alert, InputGroup, Label } from '@bootstrap-styled/v4';
import Button from '../../Common/Button';

import AlertList from '../../Common/AlertList';
import routes from '../../../constants/routes';
import jobTypeOptions from '../constants';
import Layout from '../../Common/Layout';
import { StyledFormGroup, StyledFieldWrapper, StyledLabel, StyledSelect } from '../styles';
import CustomAutocomplete from '../../Common/CustomAutocomplete';
import { throttle, intersectionBy } from 'lodash';
import Breadcrumbs from '../../Common/Breadcrumb';
import { Loader } from '../../Common/Loader';
import { StyledSingleOption } from './styles';
import {
	getCustomers,
	getJobType,
	getOffices,
	getOptionObject,
	getRegions,
	getServiceLines,
	getAssetDefinitions
} from '../utils';
import TextField from '../../Common/TextField';
import { CustomAceEditor } from '../../Common/AceCodeEditor';
import ControlPanel from '../../Common/ControlPanel';

const getCustomerThrottle = throttle(getCustomers);

const CustomOptionValue = ({ data, selectOption }) => (
	<div
		key={data.id}
		onClick={() => {
			selectOption(data);
		}}
	>
		<StyledSingleOption>
			{data.icon && <img src={data.icon} />}
			<b>({data.abbr})</b>
			<span>{data.label}</span>
		</StyledSingleOption>
	</div>
);

const ASSET_TYPES = {
	APPOINTMENT_ASSETS: 'appointmentAssets',
	APPOINTMENT_LOCATION: 'appointmentLocation'
};

const FREQUENCY_TYPES = ['Daily', 'Weekly', 'Monthly', 'Yearly'].map((item) => {
	return {
		id: item.toLowerCase(),
		value: item.toLowerCase(),
		label: item
	};
});

const FormDispatch = ({ dispatchRule, formName, formId, onSave, dispatchIndex }) => {
	const history = useHistory();
	const mode = useMemo(() => (dispatchRule ? 'Edit' : 'Create'), [dispatchRule]);

	const [ruleData, setRuleData] = useState(
		dispatchRule && dispatchRule.ruleData ? dispatchRule.ruleData : ''
	);
	const [originalRuleData] = useState(
		dispatchRule && dispatchRule.ruleData ? dispatchRule.ruleData : ''
	);
	const [isRuleValid, setIsRuleValid] = useState(true);
	const [serviceLines, setServiceLines] = useState(
		dispatchRule && dispatchRule.serviceLines ? dispatchRule.serviceLines : []
	);
	const [jobType, setJobType] = useState(dispatchRule ? getJobType(dispatchRule.jobType) : '');
	const [region, setRegion] = useState(dispatchRule ? dispatchRule.region : '');
	const [regionOptions, setRegionOptions] = useState([]);
	const [customer, setCustomer] = useState(dispatchRule ? dispatchRule.customer : '');
	const [office, setOffice] = useState(dispatchRule ? dispatchRule.office : '');
	const [officeOptions, setOfficeOptions] = useState([]);
	const [state, setState] = useState(dispatchRule ? dispatchRule.state : '');
	const [city, setCity] = useState(dispatchRule ? dispatchRule.city : '');
	const [assetType, setAssetType] = useState(dispatchRule ? dispatchRule.assetType : '');
	const [assetTypeOptions, setAssetTypeOptions] = useState([]);
	const [serviceFrequency, setServiceFrequency] = useState(
		dispatchRule ? dispatchRule.serviceFrequency : ''
	);
	const [serviceInterval, setServiceInterval] = useState(
		dispatchRule ? dispatchRule.serviceInterval : ''
	);

	const [errorText, setErrorText] = useState('');
	const [isErrorShown, setIsErrorShown] = useState(false);
	const [isDataSaved, setIsDataSaved] = useState(false);
	const [isDataLoading, setIsDataLoading] = useState(false);

	const [serviceDescription, setServiceDescription] = useState(
		dispatchRule && dispatchRule.serviceDescription ? dispatchRule.serviceDescription : ''
	);
	const [loadAssets, setLoadAssets] = useState(
		dispatchRule && dispatchRule.loadAssets
			? dispatchRule.loadAssets
			: ASSET_TYPES.APPOINTMENT_ASSETS
	);
	const [serviceLinesOptions, setServiceLinesOptions] = useState([]);
	const [customerOptions, setCustomerOptions] = useState([]);
	const [isCustomerLoading, setIsCustomerLoading] = useState(false);

	const onCustomerChange = async (value) => {
		if (value.length >= 2) {
			setIsCustomerLoading(true);
			const customers = await getCustomerThrottle(value);
			if (customers) {
				setCustomerOptions(
					customers.map((item) => {
						return {
							id: item.id,
							text: item.name
						};
					})
				);
			}
			setCustomer('');
			setIsCustomerLoading(false);
		} else if (!value.length) {
			setCustomer();
		}
	};

	const onCustomerSelect = async (selectedItem) => {
		setCustomer(selectedItem);
	};

	const handleCancel = useCallback(() => {
		history.push(`${routes.FORMS}/${formId}`);
	}, []);

	useEffect(() => {
		(async () => {
			setIsDataLoading(true);
			try {
				const data = await Promise.all([
					getServiceLines(),
					getOffices(),
					getRegions(),
					getAssetDefinitions()
				]);
				const serviceLineOpt = data[0].map((item) => {
					return {
						id: item.id,
						label: item.name,
						value: item.name,
						icon: item.icon,
						abbr: item.abbr
					};
				});
				const officeOpt = data[1].map((item) => getOptionObject(item));
				const regionOpt = data[2].map((item) => getOptionObject(item));
				const assetDefinitionsOpt = data[3].map((item) => {
					return { id: item.id, label: item.label, value: item.value };
				});

				setServiceLines(intersectionBy(serviceLineOpt, serviceLines, 'id'));

				setServiceLinesOptions(serviceLineOpt);
				setOfficeOptions(officeOpt);
				setRegionOptions(regionOpt);
				setAssetTypeOptions(assetDefinitionsOpt);
				setIsDataLoading(false);
			} catch (e) {
				setIsDataLoading(false);
			}
		})();
	}, []);

	const saveDispatch = () => {
		try {
			onSave({
				ruleData,
				jobType: jobType && jobType.value ? jobType.value : null,
				customer,
				serviceDescription,
				region,
				office,
				serviceLines,
				loadAssets,
				state,
				city,
				assetType,
				serviceFrequency,
				serviceInterval
			});

			history.push(`${routes.FORMS}/${formId}`);
		} catch (e) {
			setErrorText(e.message);
			setIsErrorShown(true);
		}
	};

	const deleteDispatch = useCallback(() => {
		const readyToDelete = window.confirm(
			'Are you sure, that you want to remove this dispatch rule?'
		);

		if (!readyToDelete) return;

		onSave(null);
		history.push(`${routes.FORMS}/${formId}`);
	}, []);

	const handleTestRule = useCallback(() => {
		history.push(`${routes.TEST_DISPATCH_RULE}/${formId}/${dispatchIndex}`);
	}, []);

	const isTestRuleDisabled = useMemo(() => {
		if (ruleData) {
			return ruleData !== originalRuleData;
		} else {
			return originalRuleData !== '';
		}
	}, [originalRuleData, ruleData]);

	const isSaveDisabled = useMemo(() => {
		return (
			!isRuleValid ||
			(serviceFrequency && !!!serviceInterval) ||
			(!!!serviceFrequency && serviceInterval)
		);
	}, [isRuleValid, serviceFrequency, serviceInterval]);

	return (
		<Layout.Container>
			<Layout.Content>
				<Breadcrumbs title={formName} url={`${routes.FORMS}/${formId}`} />
				<Layout.Title title={`${mode} Dispatch`} />

				{isDataLoading ? (
					<Loader />
				) : (
					<React.Fragment>
						<StyledFormGroup>
							<StyledLabel for="customer">Customer</StyledLabel>

							<StyledFieldWrapper>
								<CustomAutocomplete
									inputProps={{
										placeholder: 'All Customers',
										maxLength: 20
									}}
									options={customerOptions}
									value={customer}
									onChange={onCustomerChange}
									onSelect={onCustomerSelect}
									displayValue={'text'}
									isLoading={isCustomerLoading}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="region">Region</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									placeholder={'All Regions'}
									options={regionOptions}
									value={region}
									onChange={(selectedItem) => setRegion(selectedItem)}
									displayValue={'text'}
									isClearable={true}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="office">Office</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									placeholder={'All Offices'}
									options={officeOptions}
									value={office}
									onChange={(selectedItem) => setOffice(selectedItem)}
									displayValue={'text'}
									isClearable={true}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="jobType">Job Type</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									options={jobTypeOptions}
									placeholder="All Job Types"
									value={jobType}
									onChange={(selectedItem) => setJobType(selectedItem)}
									displayValue={'text'}
									isClearable={true}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel>Service Lines</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									isMulti={true}
									placeholder="All Service Lines"
									options={serviceLinesOptions}
									value={serviceLines}
									onChange={(selectedItem) => setServiceLines(selectedItem)}
									components={{ Option: CustomOptionValue }}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="region">Service Description</StyledLabel>

							<StyledFieldWrapper>
								<InputGroup>
									<TextField
										type="text"
										name="serviceDescription"
										id="serviceDescription"
										placeholder="All Services"
										value={serviceDescription}
										onChange={(e) => setServiceDescription(e.target.value)}
									/>
								</InputGroup>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="State">State</StyledLabel>

							<StyledFieldWrapper>
								<InputGroup>
									<TextField
										type="text"
										name="state"
										id="state"
										placeholder="All States"
										value={state}
										onChange={(e) => setState(e.target.value)}
									/>
								</InputGroup>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="city">City</StyledLabel>

							<StyledFieldWrapper>
								<InputGroup>
									<TextField
										type="text"
										name="city"
										id="city"
										placeholder="All Cities"
										value={city}
										onChange={(e) => setCity(e.target.value)}
									/>
								</InputGroup>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="assetType">Asset Type</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									options={assetTypeOptions}
									placeholder="All Asset Types"
									value={assetType}
									onChange={(selectedItem) => setAssetType(selectedItem)}
									displayValue={'text'}
									isClearable={true}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="serviceFrequency">Service Frequency</StyledLabel>

							<StyledFieldWrapper>
								<StyledSelect
									options={FREQUENCY_TYPES}
									placeholder="Any frequency"
									value={serviceFrequency}
									onChange={(selectedItem) => setServiceFrequency(selectedItem)}
									displayValue={'text'}
									isClearable={true}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="serviceInterval">Service Interval</StyledLabel>

							<StyledFieldWrapper>
								<InputGroup>
									<TextField
										type="number"
										name="serviceInterval"
										id="serviceInterval"
										value={serviceInterval}
										onChange={(e) => setServiceInterval(e.target.value)}
									/>
								</InputGroup>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="region">Load Assets</StyledLabel>

							<StyledFieldWrapper>
								<InputGroup>
									<Label check>
										<Input
											type="radio"
											name="clockedInAppointment"
											id="clockedInAppointment"
											checked={loadAssets === ASSET_TYPES.APPOINTMENT_ASSETS}
											onChange={(e) =>
												setLoadAssets(ASSET_TYPES.APPOINTMENT_ASSETS)
											}
										/>{' '}
										Load only assets associated with services on the clocked-in
										appointment
									</Label>
								</InputGroup>
								<InputGroup>
									<Label check>
										<Input
											type="radio"
											name="clockedInAppointment"
											id="clockedInAppointment"
											checked={
												loadAssets === ASSET_TYPES.APPOINTMENT_LOCATION
											}
											onChange={(e) =>
												setLoadAssets(ASSET_TYPES.APPOINTMENT_LOCATION)
											}
										/>{' '}
										Load all assets at the clocked-in appointment's location
									</Label>
								</InputGroup>
							</StyledFieldWrapper>
						</StyledFormGroup>

						<StyledFormGroup>
							<StyledLabel for="ruleData">Dispatch Mapping</StyledLabel>

							<StyledFieldWrapper>
								<CustomAceEditor
									id="destinationRule"
									mode={'jsx'}
									placeholder="Code goes here"
									value={ruleData}
									onChange={(value, isValid) => {
										setRuleData(value);
										setIsRuleValid(isValid);
									}}
								/>
							</StyledFieldWrapper>
						</StyledFormGroup>
					</React.Fragment>
				)}

				<AlertList>
					<Alert
						isOpen={isErrorShown}
						color={'danger'}
						autoHideDuration={3000}
						onClick={() => setIsErrorShown(false)}
					>
						{errorText}
					</Alert>
					<Alert
						isOpen={isDataSaved}
						color={'success'}
						autoHideDuration={3000}
						onClick={() => setIsDataSaved(false)}
					>
						{'Dispatch rule saved'}
					</Alert>
				</AlertList>
			</Layout.Content>

			{isDataLoading ? null : (
				<ControlPanel>
					<Button className={'mr-3'} onClick={handleCancel} color="secondary">
						Cancel
					</Button>

					{mode === 'Edit' ? (
						<React.Fragment>
							<Button
								className={'mr-3'}
								disabled={isTestRuleDisabled}
								onClick={handleTestRule}
								color="info"
							>
								Test Rule
							</Button>
							<Button className={'mr-3'} onClick={deleteDispatch} color="danger">
								Delete Dispatch Rule
							</Button>
						</React.Fragment>
					) : null}

					<Button disabled={isSaveDisabled} onClick={saveDispatch}>
						Save Dispatch Rule
					</Button>
				</ControlPanel>
			)}
		</Layout.Container>
	);
};

FormDispatch.propTypes = {
	formId: PropTypes.string.isRequired,
	formName: PropTypes.string.isRequired,
	onSave: PropTypes.func.isRequired,
	dispatchRule: PropTypes.shape({
		ruleData: PropTypes.string,
		region: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		serviceDescription: PropTypes.string,
		customer: PropTypes.shape({}),
		jobType: PropTypes.string,
		office: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		serviceLines: PropTypes.array,
		state: PropTypes.string,
		city: PropTypes.string,
		assetType: PropTypes.shape({}),
		serviceFrequency: PropTypes.string,
		serviceInterval: PropTypes.number
	})
};

export default FormDispatch;
